import React from "react";
import Profile from "./Profile";
import TableInfo from "./TableInfo";

export default function ContentPage(props) {
  return (
    <div>
          <Profile
            onInsert={props.onInsert}
          />
          <TableInfo
            data={props.data}
            onDelete={props.onDelete}
          />
    </div>
  );
}
