import React, { useEffect, useState } from "react";
import { Tabs, Tab, DropdownButton, Dropdown, Card, ButtonGroup, Modal, Button } from "react-bootstrap";
import { FaEye, FaEdit, FaTrash } from "react-icons/fa";
import _ from "lodash";
import moment from 'moment'
import 'moment/locale/km'
import { AiOutlineUser } from "react-icons/ai";

export default function TableInfo(props) {

  let { data, onDelete } = { ...props }

  const pageSize = 5
  const [paginatedPosts, setPaginatedPosts] = useState(_(data).slice(0).take(pageSize).value());

  const [showModal, setShowModal] = useState(false)
  const [modalInfo, setModalInfo] = useState([]);

  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  useEffect(() => {
    setPaginatedPosts(_(data).slice(0).take(pageSize).value())
  }, [data.length])

  const pageCount = data ? Math.ceil(data.length / pageSize) : 0
  // if (pageCount === 0) return null;
  const pages = _.range(1, pageCount + 1)
  const [currentPage, setCurrentPage] = useState(1)

  const pagination = (pageNumber) => {
    setCurrentPage(pageNumber)
    const startIndex = (pageNumber - 1) * pageSize;
    const paginatedPost = _(data).slice(startIndex).take(pageSize).value()
    setPaginatedPosts(paginatedPost)
  }

  const onDeleteItem = (id) => {
    onDelete(id);
  }

  const onViewItem = (item) => {
    setModalInfo(item);
    toggleTrueFalse()
  }

  const toggleTrueFalse = () => {
    setShowModal(handleShow)
  }

  // Modal
  const ModalContent = () => {
    handleShow();
    return (
      <div>
        {/* Modal */}
        <Modal show={show} onHide={handleClose}>
          <Modal.Header>
            <Modal.Title>Person Info</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            {/* username */}
            <div className="mb-3">
              <label htmlFor="username-modal" className="form-label">
                Username
              </label>
              <input
                type="text"
                className="form-control"
                value={modalInfo.username}
                readOnly
              />
            </div>
            {/* email */}
            <div className="mb-3">
              <label htmlFor="email-modal" className="form-label">
                Email
              </label>
              <input
                type="email"
                className="form-control"
                value={modalInfo.email}
                readOnly
              />
            </div>
            {/* Gender */}
            <div className="mb-3">
              <label className="form-label me-3">Gender : </label>
              <div className="form-check form-check-inline">
                <input
                  className="form-check-input checked"
                  type="radio"
                  checked
                  onChange={(e) => { }}
                />
                <label className="form-check-label" htmlFor="rd-male">
                  {modalInfo.gender}
                </label>
              </div>
            </div>
            {/* Job */}
            <div className="mb-3" id="job">
              <label className="form-label me-3">Job : </label>
              {modalInfo.job.map((temp, key) => (
                <div key={key} className="form-check form-check-inline">
                  <input
                    className="form-check-input"
                    type="checkbox"
                    checked
                    onChange={(e) => { }}
                  />
                  <label className="form-check-label" htmlFor="chk-student">
                    {temp}
                  </label>
                </div>
              ))}
            </div>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="danger" onClick={handleClose}>
              Close
            </Button>
          </Modal.Footer>
        </Modal>
      </div>
    )
  }

  return (
    <div>
      <h3 className="mt-5 mb-3">Table Account</h3>
      <Tabs defaultActiveKey="Table" id="uncontrolled-tab-example">
        <Tab eventKey="Table" title="Table">
          <table className="table" id="table-data">
            <thead>
              <tr className="bg-success text-light">
                <th scope="col" style={{ width: "8%" }}>#</th>
                <th scope="col" style={{ width: "12%" }}>Name</th>
                <th scope="col" style={{ width: "15%" }}>Email</th>
                <th scope="col" style={{ width: "10%" }}>Gender</th>
                <th scope="col" style={{ width: "10%" }}>Job</th>
                <th scope="col" style={{ width: "15%" }}>Created at</th>
                <th scope="col" style={{ width: "15%" }}>Updated at</th>
                <th scope="col" style={{ width: "20%" }}>Action</th>
              </tr>
            </thead>
            <tbody>
              {paginatedPosts.map((item, index) => (
                <tr key={index}>
                  <td>{item.id}</td>
                  <td>{item.username}</td>
                  <td>{item.email}</td>
                  <td>{item.gender}</td>
                  <td>
                    <ul>
                      {item.job.map((job, key) => (
                        <li key={key}>{job}</li>
                      ))}
                    </ul>
                  </td>
                  <td>{moment(item.created).startOf('').fromNow()}</td>
                  <td>{item['updated'] == undefined ? "" : moment(item.updated).startOf('').fromNow()}</td>
                  <td>
                    <div id="table-button">
                      {/* btn show */}
                      <button type="button" className="btn btn-primary mb-2 btn-view" onClick={() => onViewItem(item)} >
                        <FaEye />
                      </button>
                      {show ? <ModalContent /> : null}
                      {/* btn update */}
                      <button type="button" className="btn btn-light mb-2 btn-update">
                        <FaEdit />
                      </button>
                      {/* btn delete */}
                      <button type="button" className="btn btn-danger mb-2" onClick={() => onDeleteItem(item.id)}>
                        <FaTrash />
                      </button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </Tab>
        <Tab eventKey="Card" title="Card">
          <div className="d-flex justify-content-between flex-wrap">
            {paginatedPosts.map((item, index) => (
              <Card key={index} border="primary" style={{ width: '15rem' }} className="mt-4 mb-3">
                <Card.Header className="text-center">
                  <DropdownButton as={ButtonGroup} variant="success" title="Action" id="bg-nested-dropdown" className="ml-5 btn-action">
                    <Dropdown.Item eventKey="1" onClick={() => onViewItem(item)}><FaEye /> View</Dropdown.Item>
                    <Dropdown.Item eventKey="2"><FaEdit /> Update</Dropdown.Item>
                    <Dropdown.Item eventKey="3" onClick={() => onDeleteItem(item.id)}><FaTrash /> Delete</Dropdown.Item>
                  </DropdownButton>
                </Card.Header>
                <Card.Body>
                  <Card.Title>{item.username}</Card.Title>
                  <Card.Text>
                    Job
            </Card.Text>
                  <ul>
                    {item.job.map((job, id) => (
                      <li key={id}>{job}</li>
                    ))}
                  </ul>
                </Card.Body>
                <Card.Footer>
                  {moment(item.created).startOf('').fromNow()}
                </Card.Footer>
              </Card>
            ))}
          </div>
        </Tab>
      </Tabs>
      <div style={data.length === 0 ? { display: "block" } : { display: "none" }}>
        {/* icon */}
        <div className="text-center mb-3">
          <AiOutlineUser fontSize="100px" className="mb-2" />
          <h5>No Data Show</h5>
        </div>
      </div>
      <div className="mb-5">
        <ul className="pagination" style={{ display: 'flex', justifyContent: 'center' }}>
          {
            pages.map((page) =>
              <li className="page-link" style={{ display: 'block' }}
                style={page === currentPage ? { backgroundColor: 'DodgerBlue', color: "white" } : { backgroundColor: 'white' }}
                onClick={() => pagination(page)}
              >{page}</li>
            )
          }
        </ul>
      </div>
    </div>
  );
}
