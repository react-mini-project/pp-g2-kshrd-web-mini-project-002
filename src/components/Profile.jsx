import React from "react";
import "bootstrap/dist/css/bootstrap.css";

export default function Profile(props) {

  let onSave = () => {
    if (emptyFieldValidation() || emailValidation()) {
      return;
    }
    let username = document.querySelector("#usernameInput");
    let email = document.querySelector("#emailInput");
    let male = document.querySelector("#rd-male")
    let student = document.querySelector('#chk-student');
    let developer = document.querySelector('#chk-developer');
    let teacher = document.querySelector('#chk-teacher');
    let id = uuid();
    let gender_value;
    let job = findJob(student, teacher, developer);
    let createdDate = new Date();
    if (male.checked) {
      gender_value = male.value;
    } else {
      gender_value = document.querySelector("#rd-female").value;
    }

    let obj = {
      id : id,
      username: username.value.trim(),
      email: email.value.trim(),
      gender: gender_value,
      job: job,
      created : createdDate,
      updated : "",
      selected: false
    };
    props.onInsert(obj);
    clearInput(username, email, male, student, teacher, developer);
  };

  // uuid
  let uuid = () => {
    return 'xxx-xx'.replace(/[xy]/g, function(c) {
      let r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
      return v.toString(16);
    });
  }

  // Job
  let findJob = (student, teacher, developer) => {
    let job = [];
    if(student.checked){
      job.push(student.value);
    }
    if(teacher.checked){
      job.push(teacher.value);
    }
    if(developer.checked){
      job.push(developer.value);
    }
    return job;
  }

  // empty field validation
  let emptyFieldValidation = () => {
    let email = document.getElementById("emailInput");
    let emailHelp = document.getElementById("emailHelp");
    let username = document.getElementById("usernameInput");
    let userHelp = document.getElementById("userHelp");
    let isEmpty = true;

    if (username.value.trim() === "") {
      username.classList.add("is-invalid");
      userHelp.innerText = "Username can not be empty.";
    } else if (!userRegex.test(email.value)) {
      email.classList.add("is-invalid");
      emailHelp.innerText = "Invalid email address.";
    } else {
      username.classList.remove("is-invalid");
      userHelp.innerText = "";
    }
    if (email.value.trim() === "") {
      email.classList.add("is-invalid");
      emailHelp.innerText = "Email can not be empty.";
    } else if (!emailRegex.test(email.value)) {
      email.classList.add("is-invalid");
      emailHelp.innerText = "Invalid email address.";
    } else {
      email.classList.remove("is-invalid");
      emailHelp.innerText = "";
    }
    if (username.value.trim() === "" || email.value.trim() === "") {
      isEmpty = true;
    } else {
      isEmpty = false;
    }
    return isEmpty;
  };

  // clear input
  let clearInput = (username, email, male, student, teacher, developer) => {
    username.value = "";
    email.value = "";
    if(student.checked){
      student.checked = false
    }
    if(teacher.checked){
      teacher.checked = false
    }
    if(developer.checked){
      developer.checked = false
    }
    if(!male.checked){
      male.checked = true;
    }
  };

  // cancel input
  let onCancel = () => {
    let email = document.getElementById("emailInput");
    let username = document.getElementById("usernameInput");
    email.value = "";
    username.value = "";
    username.classList.remove("is-invalid")
    email.classList.remove("is-invalid");
  }

  // email invalid
  let emailRegex = /^[^\s@]+@([^\s@.,]+\.)+[^\s@.,]{2,}$/;
  let emailValidation = () => {
    let isValid = false;
    let email = document.querySelector("#emailInput");
    let emailHelp = document.querySelector("#emailHelp");
    if (!emailRegex.test(email.value)) {
      email.classList.add("is-invalid");
      emailHelp.innerText = "Invalid email address.";
      isValid = true;
    } else {
      email.classList.remove("is-invalid");
      emailHelp.innerText = "";
      isValid = false;
    }
    if (email.value === "") {
      email.classList.remove("is-invalid");
      emailHelp.innerText = "";
    }
    return isValid;
  };

  // username invalid
  let userRegex = /^[^0-9]+$/;
  let usernameValidation = () => {
    let username = document.querySelector("#usernameInput");
    let userHelp = document.querySelector("#userHelp");
    let isValid = false;
    if (!userRegex.test(username.value)) {
      username.classList.add("is-invalid");
      userHelp.innerText = "Invalid email address.";
      isValid = true;
    } else {
      username.classList.remove("is-invalid");
      userHelp.innerText = "";
      isValid = false;
    }
    if (username.value.trim() === "" || username.value.trim() !== "") {
      username.classList.remove("is-invalid");
      userHelp.innerText = "";
    }
    return isValid;
  };

  return (
    <div>
      <form>
        {/* icon */}
        <div className="mb-3">
          <h3>Person Info</h3>
        </div>
        <div className="row">
          <div className="col-lg-6 col-sm-12">
            {/* username */}
            <div className="mb-3">
              <label htmlFor="username" className="form-label">
                Username
              </label>
              <input
                type="text"
                className="form-control"
                id="usernameInput"
                aria-describedby="userlHelp"
                placeholder="Username"
                onChange={usernameValidation}
              />
              <p id="userHelp" className="form-text invalid-feedback"></p>
            </div>
            {/* email */}
            <div className="mb-3">
              <label htmlFor="emailInput" className="form-label">
                Email
              </label>
              <input
                type="email"
                className="form-control"
                id="emailInput"
                aria-describedby="emailHelp"
                placeholder="Example@gmail.com"
                onChange={emailValidation}
              />
              <p id="emailHelp" className="form-text invalid-feedback"></p>
            </div>
          </div>
          <div className="col-lg-6 col-sm-12">
            {/* Gender */}
            <label className="form-label">Gender</label>
            <div className="mb-3" id="gender">
              <div className="form-check form-check-inline">
                <input
                  className="form-check-input checked"
                  type="radio"
                  id="rd-male"
                  value="male"
                  name="rd-gender"
                  onChange={(e) => { }}
                  checked
                />
                <label className="form-check-label" htmlFor="rd-male">
                  male
                </label>
              </div>
              <div className="form-check form-check-inline">
                <input
                  className="form-check-input"
                  type="radio"
                  id="rd-female"
                  value="female"
                  name="rd-gender"
                />
                <label className="form-check-label" htmlFor="rd-female">
                  female
                </label>
              </div>
            </div>
            {/* Job */}
            <label className="form-label mt-2">Job</label>
            <div className="mb-3" id="job">
              <div className="form-check form-check-inline">
                <input
                  className="form-check-input"
                  type="checkbox"
                  id="chk-student"
                  value="Student"
                  name="chk-job"
                  onChange={(e) => { }}
                />
                <label className="form-check-label" htmlFor="chk-student">
                  Student
                </label>
              </div>
              <div className="form-check form-check-inline">
                <input
                  className="form-check-input"
                  type="checkbox"
                  id="chk-teacher"
                  value="Teacher"
                  name="chk-job"
                  onChange={(e) => { }}
                />
                <label className="form-check-label" htmlFor="chk-teacher">
                  Teacher
                </label>
              </div>
              <div className="form-check form-check-inline">
                <input
                  className="form-check-input"
                  type="checkbox"
                  id="chk-developer"
                  value="Developer"
                  name="chk-job"
                  onChange={(e) => { }}
                />
                <label className="form-check-label" htmlFor="chk-developer">
                  Developer
                </label>
              </div>
            </div>
          </div>
        </div>
        <button type="button" className="btn btn-primary mb-3" onClick={onSave}> 
          Save
        </button> {' '}
        <button type="button" className="btn btn-danger mb-3" onClick={onCancel}> 
          Cancel
        </button>
      </form>
    </div>
  );
}
