import "bootstrap/dist/css/bootstrap.css";
import React, { Component } from "react";
import { Container } from "react-bootstrap";
import ContentPage from "./components/ContentPage";
import NavBar from './components/NavBar'
import './app.css'

export default class App extends Component {

  constructor() {
    super();
    this.state = {
      data: [
        {
          id : this.uuid(),
          username : 'Santarak',
          gender : 'male', 
          email : 'k.santarak@gmail.com',
          job : ["Student", "Developer"],
          created : "Fri May 28 2021 23:17:20 GMT+0700 (Indochina Time)",
          selected : false
        },
        {
          id : this.uuid(),
          username : 'Leangkhim',
          gender : 'female', 
          email : 'leangkhim@gmail.com',
          job : ["Student", "Developer"],
          created : "Fri May 28 2021 23:17:20 GMT+0700 (Indochina Time)",
          selected : false
        },
        {
          id : this.uuid(),
          username : 'Parinhnha',
          gender : 'male', 
          email : 'parinhnha@gmail.com',
          job : ["Developer", "Teacher"],
          created : "Fri May 28 2021 23:17:20 GMT+0700 (Indochina Time)",
          selected : false
        },
        {
          id : this.uuid(),
          username : 'Sokkry',
          gender : 'male', 
          email : 'Sokkry@gmail.com',
          job : ["Developer", "Teacher"],
          created : "Fri May 28 2021 23:17:20 GMT+0700 (Indochina Time)",
          selected : false
        }
      ],
    };
  }

    // uuid
    uuid = () => {
      return 'xxx-xx'.replace(/[xy]/g, function(c) {
        let r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
      });
    }

  onInsert = (props) => {
    let data = this.state.data;
    data.push(props);
    this.setState({
      data: data
    });
  }

  onDelete = (deletedId) => {
    let data = this.state.data;
    data= data.filter(item => {
      return item.id !== deletedId;
    });
    this.setState({
      data : data
    })
  };

  onUpdate = (updateId) => {
    
  }

  render() {
    return (
      <div>
        <NavBar/>
        <Container>
          <ContentPage
            data={this.state.data}
            onInsert={this.onInsert}
            onDelete={this.onDelete}
            onUpdate={this.onUpdate}
          />
        </Container>
      </div>
    );
  }
}
